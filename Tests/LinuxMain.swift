import XCTest

import BidentTests

var tests = [XCTestCaseEntry]()
tests += BidentTests.allTests()
XCTMain(tests)
