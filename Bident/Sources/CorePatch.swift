//
//  CorePatch.swift
//  Bident
//
//  Created by Narumi Inada on 2021/05/11.
//  Copyright © 2021 Narumi Inada. All rights reserved.
//


import SwiftUI



protocol CorePatch : Patch {}

//extension Test : PatchView {
//
//    /**
//     Custom definition of the Patch View
//     */
//    public var body : some View {
//        Text("Original").foregroundColor(.white)
//    }
//}

public struct Test : Patch {
    public func execute(context: inout PropagateContext) {
        
    }
    
    public var timeMode: PatchTimeMode {.idle}
    
    public var patchType: PatchType {.consumer}
    
    var label: String?
    
    public var info: PatchInfo = PatchInfo()
    
    
    @Input var A : Int = 1
    @Input var B : Double = 10.0
    
    @Output var C : Double = 0.0
    
    public init() {
        
    }
    
    public func execute() {
        
    }
    
    
}

public struct Adder : Patch {
    public var patchType: PatchType { PatchType.processor }
    
    public var timeMode: PatchTimeMode = .none
    
    public var info: PatchInfo = .init()
    
    @Input var A : Float = 0.0
    @Input var B : Float = 0.0
    
    @Output var Output : Float = 0.0
    
    public init() {}
    
    public func execute(context: inout PropagateContext) {
        Output = A + B
    }
    
    
}






