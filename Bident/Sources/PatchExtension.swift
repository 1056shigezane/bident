//
//  PatchExtension.swift
//  Bident
//
//  Created by Narumi Inada on 2021/05/19.
//  Copyright © 2021 Narumi Inada. All rights reserved.
//

import Foundation


public struct FrameInfo {
    var id = UUID()
    var isIterating : Bool = false
    var currentIndex : Int = -1
    var iterations : Int = -1
    
    var waitingPatchesForComplete : [Patch] = []
    var isAlreadyCalled : Bool = false
    var isAlreadyCompleted : Bool = false
    
}

public struct FrameID : Codable {
    
    var id : UUID = UUID()
    var isIterating : Bool = false
    var index : Int = -1
    
    static func ==(l:FrameID,r:FrameID) -> Bool {
        l.id == r.id && l.index == r.index
    }
    
}

public struct PatchInfo {
    
    
    public let id = UUID()
    public lazy var queue = DispatchQueue.init(label: id.uuidString)
    public var waitingompletionsForComplete : [(_ isIterating:Bool)->Void] = []
    
    public var timeBaseIsExternal : Bool = false
    @Input public var patchTime = 0.0
    
    public var lastExecutedFrameID : FrameID = .init()
    public var lastCalledFrameID : FrameID = .init()
    
    public var order : Int = -1
    
    public var label : String?
    
    public var point : CGSize = .zero
    
    
    public init(){
        
    }
    
}




public protocol OutputDefinable {
    var customOutputs : [ParameterWrapper] {get set}
    //mutating func addOutput(parameter:ParameterWrapper)
}

public protocol InputDefinable {
    var customInputs : [ParameterWrapper] { get set}
    //mutating func addInput(parameter:ParameterWrapper)
}

public typealias ParameterDefinable = OutputDefinable & InputDefinable


public enum PatchType {
    case provider
    case processor
    case consumer
    case undefined
    
    case audioConsumer
    case audioProcessor
    case audioGenerator
    
    case macro
}

public enum PatchTimeMode {
    case none
    case time
    case idle
}


extension Patch {
    
    var moduleName : String { String(describing: Self.self) }
    
    public var id : UUID {get{ info.id }}
    var queue : DispatchQueue {mutating get{ info.queue }}
    
    func parent(root:Macro) -> Macro? {
        
        guard root.id != self.id else{ return nil }
        
        var states : [Macro] = [root]
        
        while !states.isEmpty {
            
            for state in states {
                
                if state.isParentOf(patch: self) {
                    return state
                }else{
                    
                    let macros : [Macro] = state.children.compactMap{
                        if $0 is Macro {
                           return ($0 as! Macro)
                        }else{return nil}
                    }
                    
                    states.append(contentsOf: macros)
                }
                
                if let index = states.firstIndex(where: { child in
                    child.id == state.id
                }) {
                    states.remove(at:index)
                }
             }
            
        }
        print("Broken: after Parent()")
        return nil
        
    }
    
    
    
    
    func parameter(for id:UUID) -> ParameterWrapper? {
        
        for child in Mirror(reflecting: self).children {
            
            guard
                let wrapper = child.value as? ParameterWrapper,
                wrapper.id == id
                else{ continue}
            
            return wrapper
        }
        
        return nil
    }
    
    func hasParameter(for param:ParameterWrapper) -> Bool {
        parameter(for:param.id) != nil
    }
    

    
 }


//extension Test : PatchView {
//
//    /**
//     Custom definition of the Patch View
//     */
//    public var body : some View {
//        Text("Original").foregroundColor(.white)
//    }
//}








public extension Patch {
    

  
    private func initialInputs(set:Set<ParameterWrapperType>) -> [(label:String,parameter:ParameterWrapper)] {
        var inputs : [(label:String,parameter:ParameterWrapper)] = []
        
        if timeMode == .time, info.timeBaseIsExternal, set.contains(.input) {
            let pt = info.$patchTime
            inputs.append( ("patchTime", pt) )
            
        }
        
        Mirror(reflecting: self).children.forEach { child in
            
            guard case let (label?,value) = child,
                let wrapper = value as? ParameterWrapper,
                set.contains(wrapper.portType)
                else{return}
            
            inputs.append((label: label, parameter: wrapper))
            
        }
        return inputs
    }
    
    
    func parameters(for set: Set<ParameterWrapperType>) -> [(label:String,parameter:ParameterWrapper)] {
        
        return initialInputs(set: set)
        
    }
}




public extension Patch where Self : InputDefinable {
    
    func parameters(for set: Set<ParameterWrapperType>) -> [(label:String, parameter: ParameterWrapper)] {
        
        var inputs = initialInputs(set: set)
        
        if set.contains(.customInput) {
            
            inputs.append(contentsOf: customInputs.map { ($0.displayName,$0) })
        }
        
        return inputs
        
    }
    
}

public extension Patch where Self : OutputDefinable {
    
    func parameters(for set: Set<ParameterWrapperType>) -> [(label:String, parameter: ParameterWrapper)] {
        
        var inputs = initialInputs(set: set)
        
        if set.contains(.customOutput) {
            
            inputs.append(contentsOf: customOutputs.map{ ($0.displayName,$0) })
            
        }
        
        return inputs
        
    }
    
}

public extension Patch where Self : ParameterDefinable {
    
    func parameters(for set: Set<ParameterWrapperType>) -> [(label:String, parameter: ParameterWrapper)] {
        
        var inputs = initialInputs(set: set)
        
        if set.contains(.customInput) {
            
            inputs.append(contentsOf: customInputs.map { ($0.displayName,$0) })
            
        }
        
        if set.contains(.customOutput) {
            
            inputs.append(contentsOf: customOutputs.map{ ($0.displayName,$0) })
            
        }
        
        return inputs
        
    }
    
}



