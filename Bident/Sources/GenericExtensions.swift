//
//  GenericExtensions.swift
//  Bident
//
//  Created by Narumi Inada on 2020/03/20.
//  Copyright © 2020 Narumi Inada. All rights reserved.
//

import Foundation


/**
 
 */

public protocol Parametable {
    static func instantiate(from: Data) throws -> Self
    func encoded() -> Data?
    
    static var compatibilities : [Parametable.Type] {get}// = [Int.self,Doub]
    static func cast(from: UnsafeMutablePointer<Parametable>) -> Self?
}





public extension Parametable where Self : Codable {
    
    static func instantiate(from: Data) throws -> Self { return try JSONDecoder().decode(Self.self, from: from) }
    func encoded() -> Data? { try? JSONEncoder().encode(self) }
    
}



extension Int : Parametable {
    public static var compatibilities: [Parametable.Type] {
        [Int.self,Float.self]
    }
    
    public static func cast(from: UnsafeMutablePointer<Parametable>) -> Int?  {
        switch from.pointee {
        case let v as Int: return v
            
        case let v as Float: return Int(v)
            
        case let v as Double: return Int(v)
            
       // case let v as String: return Int(v)
            
        default: return nil
        }
    }
}

extension Double : Parametable {
    public static var compatibilities: [Parametable.Type] {
        []
    }
    
    public static func cast(from: UnsafeMutablePointer<Parametable>) -> Double? {
     return nil
    }
    
    
    
    
    
}

extension Float : Parametable {
    public static var compatibilities: [Parametable.Type] {
        []
    }
    
    public static func cast(from: UnsafeMutablePointer<Parametable>) -> Float? {
        return nil
    }
    
    
    
    
}








extension String {
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
      self = self.capitalizingFirstLetter()
    }
}



//MARK:- CoreGraphics

import CoreGraphics

extension CGSize {
    static func +(l:CGSize,r:CGSize) -> CGSize { CGSize(width: l.width + r.width, height: l.height + r.height) }
    static func -(l:CGSize,r:CGSize) -> CGSize { CGSize(width: l.width - r.width, height: l.height - r.height) }
    
    var point : CGPoint { CGPoint(x: width, y: height) }
        
}

extension CGPoint {
    static func +(l:CGPoint,r:CGPoint) -> CGPoint { CGPoint(x: l.x + r.x, y: l.y + r.y) }
    static func -(l:CGPoint,r:CGPoint) -> CGPoint { CGPoint(x: l.x - r.x, y: l.y - r.y) }
    
    static func *(l:CGPoint,r:CGFloat) -> CGPoint {
        CGPoint(x: l.x * r, y: l.y * r)
    }
    
    var size : CGSize {CGSize(width: x, height: y)}
    
    
}
