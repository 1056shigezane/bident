//
//  PatchPlugInInterface.swift
//  Bident
//
//  Created by Narumi Inada on 2021/05/17.
//  Copyright © 2021 Narumi Inada. All rights reserved.
//

import SwiftUI

public protocol PlugIn {}

public class PlugInManager : ObservableObject {
    static var shared = PlugInManager()
    private init(){}
    
    @State var patchPlugInInterface : PatchPlugInInterface = .init()
}

extension Patch {
    public var plugIn : PatchPlugIn {
        PatchPlugIn(plugIn: self)
    }
}

public struct PatchPlugIn : PlugIn, Identifiable {
    public let id = UUID()
    var plugIn : Patch
    
}


public protocol PlugInInterface {
    
    associatedtype PlugInType = PlugIn

    var plugInArray : [PlugInType] {get set}

    mutating func load(plugIn:PlugInType)
    
}

public struct PatchPlugInInterface : PlugInInterface {
    public var plugInArray: [PatchPlugIn] = [
        MacroPatch().plugIn,
        Test().plugIn,
        Adder().plugIn
        
    ]
    
    
    mutating public func load(plugIn: PatchPlugIn) {
        
        plugInArray.append(plugIn)
        
    }
}



public struct PatchPlugInView : View {
    
    @State public var interface : PatchPlugInInterface = PlugInManager.shared.patchPlugInInterface
    
    
    var plugInSelected : (Patch) -> Void
    
    @Environment(\.presentationMode) var presentationMode
    
    public var body : some View {
        List(interface.plugInArray){ plug in
            Text(plug.plugIn.moduleName)
                .onTapGesture {
                    plugInSelected(plug.plugIn)
                    presentationMode.wrappedValue.dismiss()
            }
        }
    }
}
