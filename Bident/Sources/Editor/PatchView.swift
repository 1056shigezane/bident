//
//  PatchView.swift
//  Bident
//
//  Created by Narumi Inada on 2020/03/24.
//  Copyright © 2020 Narumi Inada. All rights reserved.
//

import Foundation
import SwiftUI

public enum PatchViewType : DynamicProperty {
    case patch
    case own
    case org
    
}

public struct ParameterRepresent : Identifiable {
    public let paramType : ParameterWrapperType
    public let id : UUID
    public let label : String
    
    
}

extension String {
    func labelized() -> String {
        
        let str = self.first == "_" ? String(self.dropFirst()) : self
        
        if let f = str.first?.uppercased() {
            return f + str.dropFirst()
        }
        return str
        
    }
}

public struct PatchRepresent : Identifiable {
    
    public let id : UUID
    
    public let label : String?
    
    public let moduleName : String
    
    public let inputs : [ParameterRepresent]
    
    public let outputs : [ParameterRepresent]
    
    //@Binding var point : CGPoint
}

public extension Patch {
    
    static var viewType : PatchViewType {return .patch}
    var viewType : PatchViewType {return Self.viewType}
    
    
    
    var insertView : some View { Text("Exception - declaire insertview ") }
    var view : some View { Text("Exception - declaire view ") }
    
    
    func parameterRepresents(for set: Set<ParameterWrapperType>) -> [ParameterRepresent] {
        
        var inputs : [ParameterRepresent] = []
        if timeMode == .time, info.timeBaseIsExternal, set.contains(.input) {
            
            
          //  inputs.append( ParameterRepresent(paramType: .input, id: info.$patchTime.id, label: "Patch Time") )
            
        }
        
        Mirror(reflecting: self).children.forEach { child in
            
            guard case let (label?,value) = child,
                let wrapper = value as? ParameterWrapper,
                set.contains(type(of:wrapper).portType)
                else{return}
            
            let rep = ParameterRepresent(paramType: .input, id: wrapper.id, label: label.labelized() )
            
            inputs.append( rep )
            
        }
        return inputs
    }
    
    
    
    
    var represent : PatchRepresent {
        
        PatchRepresent(id: self.id, label: self.info.label, moduleName: self.moduleName, inputs: parameterRepresents(for: [.input, .customInput]), outputs: parameterRepresents(for: [.output, .customOutput]) )
        
    }
    
}
/*
public class Test : Patch {
    public func propagete(ctx: inout PropagationContext) {
        
    }
    
    
    
    public static var patchType: PatchType = .consumer
    public static var timeMode: PatchTimeMode = .time
    
    public var info: PatchInfo = .init()
    public var label: String? = nil
    
    
    @Input var testDouble : Double = 1.0
    @Input var testInt : Int = 1
    
    @Output var testOut : Double = 1.0
    
    public init(){}
    
    public func execute() {
        
    }
    
    
    
}


public struct PatchView : View {
    
    
    let patch : PatchRepresent
    
    @State var point : CGSize = .zero
    var positionDidChange : (CGSize) -> Void
    
    @State var draggingBgan : Bool = true
    @State var startPoint : CGSize = .zero
    
    
    public init(patch:PatchRepresent,point:CGSize, positionDidChange:@escaping (CGSize) -> Void){
        self.patch = patch
        self.positionDidChange = positionDidChange
        self.point = point
        
    }
    
    public var body : some View {
        
        let drag = DragGesture(minimumDistance: 0, coordinateSpace: .global).onChanged { value in
            if self.draggingBgan {
                self.draggingBgan = false
                self.startPoint = self.point
            }
            
            self.point = self.startPoint + value.translation
            
            self.positionDidChange(self.point)
            print(value.translation,self.point,self.startPoint)
        }.onEnded { _ in
            self.draggingBgan = true
        }
        
        return Group{
            
            patchView
            
        }
        .padding()
        .background(Color.black)
        .cornerRadius(10)
        .position(point.point)
        .gesture(drag)
        //.offset(x: point.width, y: point.height)
        
        
        
    }
    
    
    var patchView : some View {
        
        
        return VStack{
            
            Text(patch.moduleName).foregroundColor(.white)
            
            
            
            HStack(alignment: .top, spacing: 8.0){
                VStack(alignment: .leading, spacing: 8.0){
                    ForEach(patch.inputs){
                        self.inputParameter($0)
                    }
                    
                }.frame(width: nil, height: nil, alignment: .leading)
                
                VStack(alignment: .trailing, spacing: 8.0){
                    ForEach(patch.outputs){
                        self.outputParameter($0)
                    }
                    
                }
            }
            
        }.foregroundColor(.white)
    }
    
    
    func inputParameter(_ v:ParameterRepresent) -> some View {
        HStack{
            Circle().aspectRatio(1.0, contentMode: .fit)
            Text(v.label)
        }.frame(width: nil, height: 12, alignment: .leading)
    }
    
    func outputParameter(_ v:ParameterRepresent) -> some View {
        HStack{
            Text(v.label)
            Circle().aspectRatio(1.0, contentMode: .fit)
            
        }.frame( height: 12, alignment: .trailing)
    }
    
}



struct PatchView_Preview : PreviewProvider {
    
    static var test = Test()
    static var previews : some View {
        Text("CLASS")
        
       /* PatchView(patch:test.represent, point: test.info.point, positionDidChange: {
            v in test.info.point = v
        })*/
        
        //BADView(patch: test.pointer())
    }
}




*/
