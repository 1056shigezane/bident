//
//  SandboxView.swift
//  Bident
//
//  Created by Narumi Inada on 2020/03/26.
//  Copyright © 2020 Narumi Inada. All rights reserved.
//

import SwiftUI


struct Composition {
    
    var name : String = "Untitled"
    var root : Macro!
    
    
    mutating func execute(at:TimeInterval){
        root.execute()
    }
    
    init(with:URL){}
}



