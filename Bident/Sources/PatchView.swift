//
//  PatchView.swift
//  Bident
//
//  Created by Narumi Inada on 2021/05/12.
//  Copyright © 2021 Narumi Inada. All rights reserved.
//

import SwiftUI

public protocol PatchView : View, Identifiable {}

struct PatchReceptorView : PatchView, Hashable {
    
    
    static func == (lhs: PatchReceptorView, rhs: PatchReceptorView) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
            hasher.combine(id)
    }
    
    init(with:Binding<Patch>,macro m:Binding<Macro>){
        _patch = with
        _macro = m
    }
    
    @EnvironmentObject var root : RootEntity
    @Binding var macro : Macro
    @Binding var patch : Patch
    
    
    
    var id : UUID {patch.id}
    var body : some View {
        
        
        
        
        patch.viewbody(macro: macro)
            .fixedSize()
            .position(patch.info.point.point)
            .gesture(drag)

    }
    
    
    @State var gap : CGSize = .zero
    @State var isBegan : Bool = true
    
    
    var tap : some Gesture {
        TapGesture(count: 2).onEnded({
            
        
            print("Fuckin Tapp")
        })
    }
    
    
    @State var startDate : Date = Date() - 1000
    @State var lastTapDate : Date = Date() - 1000
    
    
    var drag : some Gesture {
        
        return
            
            DragGesture(minimumDistance: 0.0, coordinateSpace: .local)
            .onChanged { (value) in
                
                if isBegan {
                    
                    
                    startDate = value.time
                    gap = patch.info.point - value.startLocation.size
                }
                isBegan = false
                
                let dst = value.location.size + gap
                
                patch.info.point = dst
                
                //macro.set(patch: patch)
                
            }.onEnded { (value) in
                isBegan = true
                
                
                if value.time - startDate < 0.2 {
                    
                    if value.time - lastTapDate < 0.2 {
                        print("Double Tapped")
                        lastTapDate = value.time - 100
                        doubleTapped()
                    }else{
                        lastTapDate = value.time
                    }
                    
                }
            }
    }
    
    func doubleTapped() {
        if patch is Macro {
            root.currentPath.append(patch.id)
        }
    }
    
}

extension Date {
    static func -(l:Date,r:Date) -> TimeInterval {
        l.timeIntervalSinceReferenceDate - r.timeIntervalSinceReferenceDate
    }
}

/*
public final class PatchGestureView : UIView, UIViewRepresentable, UIGestureRecognizerDelegate {
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func makeUIView(context: Context) -> PatchGestureView { self }
    
    public func updateUIView(_ uiView: PatchGestureView, context: Context) {}
    
    public override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
    
    @Binding var patch : Patch
    @Binding var macro : Macro
    let root: RootEntity
    
    init(patch:Binding<Patch>,macro:Binding<Macro>,root:RootEntity){
        _patch = patch
        _macro = macro
        self.root = root
        super.init(frame: .zero)
        
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(doubleTapped(sender:)))
        tgr.numberOfTapsRequired = 2
        tgr.delegate = self
        self.addGestureRecognizer(tgr)
        
        let pgr = UIPanGestureRecognizer(target: self, action: #selector(pan(sender:)))
        pgr.delegate = self
        self.addGestureRecognizer(pgr)
        
        
        self.backgroundColor = .blue
    }
    
    
    
    @objc func doubleTapped(sender:UITapGestureRecognizer){
        print("doubleTapped")
    }
    
    @State var startGap : CGPoint = .zero
    
    @objc func pan(sender:UIPanGestureRecognizer){
        
        var sup = self as UIView
        while let nsup = sup.superview {
            sup = nsup
        }
        
        switch sender.state {
        
        case .began:
            
            startGap = sender.location(in: sup) - patch.info.point.point
            
            
            break
            
        case .changed:
            
            patch.info.point = sender.location(in: sup).size + startGap.size - sup.frame.center.size + self.frame.center.size
            
            print(patch.info.point)
            break
            
        case .ended:
            break
            
        default:
            
            print("Pan Default")
        
        }
        
    }
}


struct PatchGestureModifier : ViewModifier {
    
    
    @State var isBegan : Bool = true
    @State var gap : CGSize = .zero
    @Binding var patch : Patch
    
    
   
    let root: RootEntity
    let geo: GeometryProxy
    
    
    
    var drag : some Gesture {
        
        return DragGesture(minimumDistance: 0.1, coordinateSpace: .local)
            .onChanged { (value) in
                
                if isBegan {
                    print("isBegan")
                    
                    
                    gap = patch.info.point - value.startLocation.size
                }
                isBegan = false
                
                let dst = value.location.size + gap
                patch.info.point = dst
                
            }.onEnded { (value) in
                isBegan = true
                print("Drag End")
            }
    }
    
    
    func body(content: Content) -> some View {
        
        let tap = TapGesture(count: 2).onEnded({ _ in
            print("Double Tapped")
            if patch is Macro {
                root.currentPath.append(patch.id)
            }
        })//.simultaneously(with: drag)
        
        return content
            .gesture(tap)
        
    }
}

extension View {
    func dragGestureView(patch:Binding<Patch>,root:RootEntity) -> some View {
        
        GeometryReader {
            geo -> ModifiedContent<Self,PatchGestureModifier> in
            let mod = PatchGestureModifier(patch: patch, root: root, geo: geo)
            return self.modifier(mod)
        }
        
        
    }

}
 */

extension Patch {
    
    public var root : RootEntity? {
        RootEntity.verses.first(where: {
            $0.rootMacroPatch.contains(it: self)
        })
    }
    
    public func viewbody(macro:Macro) -> some View {
        
        let inputs = self.parameters(for: [.input,.customInput])
        let outputs = self.parameters(for: [.output,.customOutput])
        
        return
            VStack{
                HStack{
                    Text(self.moduleName)
                    Spacer()
                }
                
                HStack{
                    
                    VStack(alignment:.leading){
                        ForEach(inputs.indices,id:\.self) { index in
                            let input = inputs[index]
                            inputParameter(input)
                        }
                        
                        Spacer()
                    }
                    
                    Spacer().frame(width: 8, height: nil, alignment: .center)
                    
                    VStack{
                        ForEach(outputs.indices,id:\.self) { index in
                            let output = outputs[index]
                            outputParameter(output)
                        }
                        Spacer()
                    }
                }.fixedSize()
            }.padding(.all, 8)
            .background(RoundedRectangle(cornerRadius: 8)
                            .fill()
                            .foregroundColor(.blue))
            .foregroundColor(.white)
            
    }
    
    
    
    
    func inputParameter(_ input:(label:String,parameter:ParameterWrapper)) -> some View {
        HStack{
            
            
            Port(patch:self,portObserver: input.parameter.portObserver, value: input) { (src) in
                guard src.portType.isOutput else{return}
                
                switch src.portType {
                case .input, .customInput:
                    
                    break
                    
                case .output, .customOutput:
                    
                    switch src.connection {
                    
                    case .connected(_):
                        src.connection = .none
                        break
                    case .none :
                        break
                        
                    default: break
                    }
                    break
                    
                default: break
                }
                
                input.parameter.connection = .connected(Connection.ConnectionSource(parameter: src,patch: self))
                src.connection = .connected(Connection.ConnectionSource(parameter: input.parameter, patch: self))
                root?.rootMacroPatch.set(patch: self)
            }
            
            Text(input.label)
                .font(.system(size: 18, weight: Font.Weight.regular, design: .monospaced))
                .multilineTextAlignment(.leading)
                .allowsHitTesting(false)
                .fixedSize(horizontal: true, vertical: true)
                .foregroundColor(.white)
                
            
        }.frame(width: nil, height: 20, alignment: .center)
    }
    
    func outputParameter(_ output:(label:String,parameter:ParameterWrapper)) -> some View {
        HStack{
            
            Text(output.label)
                .font(.system(size: 18, weight: Font.Weight.regular, design: .monospaced))
                .multilineTextAlignment(.trailing)
                .allowsHitTesting(false)
                .fixedSize(horizontal: true, vertical: true)
                .foregroundColor(.white)
                
            
            Port(patch:self,portObserver: output.parameter.portObserver , value: output) { (src) in // TapUp
                
                switch src.portType {
                case .input,.customInput:
                    break
                    
                case .output,.customOutput:
                    
                    if case .connected(_) = src.connection {
                        src.connection = .none
                    }else if case .none = src.connection {
                        
                    }
                    break
                
                default: break
                }
                
                
                guard src.portType.isInput  else{return}
                
                
                
                output.parameter.connection = .connected(Connection.ConnectionSource(parameter: src,patch: self))
                src.connection = .connected(Connection.ConnectionSource(parameter: output.parameter, patch: self))
                root?.rootMacroPatch.set(patch: self)
                
            }.fixedSize()
        }.frame(width: nil, height: 20, alignment: .center)
    }
}





struct TranslateView: ViewModifier {
    
    
    func body(content: Content) -> some View {
            content
                .padding()
        }
    
   
}


struct PortGestureModifier : ViewModifier {
    
    @GestureState var a : Bool = false
    let parent : Macro
    let touchDown : () -> Void
    let touchUp : () -> Void
    let changed : () -> Void
    
    
    var gesture : some Gesture {
//        LongPressGesture(minimumDuration: 0.0, maximumDistance: 0.0)
//            .updating($a) { (bool, state, transaction) in
//
//        }
        
        DragGesture(minimumDistance: 0.1, coordinateSpace: .local)
            .onChanged { (value) in
            
        }
    }
    
    func body(content: Content) -> some View {
        content
    }
    
}
extension View {
    func portGesture(parent:Macro) -> some View {
        let mod = PortGestureModifier(parent: parent,
                                      touchDown: {
            
                                      },
                                      touchUp: {},
                                      changed: {})
        return self.modifier(mod)
    }
}

struct Port : View {
    let patch : Patch
    
    var portObserver : PortObserver
    
    let value : (label:String,parameter:ParameterWrapper)
    
    
    var cableDown: (_ src:ParameterWrapper) -> Void
    
    var destination : Connection.PortDestination? {
        value.parameter.connection.destination
    }
    
    @EnvironmentObject var root : RootEntity
    
    
    
    var body : some View {
        
        
        return GeometryReader{
            geo in
            view(geo: geo)
        }.aspectRatio(1.0, contentMode:.fit)
        
        
    }
    
    func view(geo:GeometryProxy) -> some View {
        
        portObserver.position = geo.frame(in: .global).center
        
        return Group{
            
            Circle()
                .fill()
                .foregroundColor(.black)
                .frame(width: 16, height: 16, alignment: .center)
                .simultaneousGesture(TapGesture().onEnded({
                    switch root.connectionSession {

                    case .none:
                        let src : Connection.ConnectionSource = Connection.ConnectionSource(parameter: value.parameter,patch: patch)

                        if src.parameter?.portType.isOutput ?? false {
                            root.connectionSession = .connected(src)
                            print("Cable Start")
                        }

                        break

                    case .initialized(_,_):
                        print("Cable Initialized")
                        break

                    case .connecting(_):

                        break

                    case .connected(let src):

                        ///Check connection request is valid
                        print("Cable Down")
                        cableDown(src.parameter!)
                        root.connectionSession = .none

                        break


                    }

                }))
                
                    
                    
        }
    }
}


extension CGRect {
    var center : CGPoint {
        origin + CGPoint(x: size.width / 2, y: size.height / 2)
    }
}
