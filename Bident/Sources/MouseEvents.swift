//
//  MouseEvents.swift
//  Bident
//
//  Created by Narumi Inada on 2021/05/13.
//  Copyright © 2021 Narumi Inada. All rights reserved.
//

import SwiftUI


final class MouseMonitor : UIView, ObservableObject {
    
    @Published var mouseLocation : CGPoint = .zero
    
    init()// Do any additional setup after loading the view.
    {
        super.init(frame: .zero)
        let hgr = UIHoverGestureRecognizer(target: self, action: #selector(handleHover(sender:)))
        addGestureRecognizer(hgr)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func handleHover(sender:UIHoverGestureRecognizer){
        mouseLocation = sender.location(in: self)
        //print("mouse",mouseLocation)
    }
}

extension MouseMonitor : UIViewRepresentable {
    func makeUIView(context: Context) -> some UIView {
        self
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
}


