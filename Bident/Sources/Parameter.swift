//
//  File.swift
//  Bident
//
//  Created by 稲田成実 on 2020/03/19.
//  Copyright © 2020 Narumi Inada. All rights reserved.
//

import Foundation
import SwiftUI
import Combine







public enum ParameterWrapperType : CaseIterable {
    
    case input
    case output
    
    case customInput
    case customOutput
    
    case passThru
    
    public var isInput : Bool {
        switch self {
        case .input,.customInput: return true
        default: return false
        }
    }
    
    var isOutput : Bool {
        switch self {
        case .output,.customOutput: return true
        default: return false
        }
    }
}

public protocol ParameterWrapper : class {
    
    var portType : ParameterWrapperType {get}
    var id : UUID {get}
    var displayName : String {get}
    var defaultName : String {get}
    var valueType : Parametable.Type {get}
    var connection : Connection {get set}
    
    var position : CGPoint {get set}
    var portObserver : PortObserver {get}
}

extension ParameterWrapper {
    /**
     May be Heavy.
     */
    public var parent : Patch? {
        for root in RootEntity.verses {
            if let target = root.rootMacroPatch.searchPatch(for: self) {
                return target
            }
        }
        return nil
    }
    
    var isConnecting : Bool {
        switch connection {
        case .initialized(let patchID, let paramID):
            return true
            
        case .connected(_):
            return true
        default:
            return false
        }
    }
    
    var connectionSource : Connection.ConnectionSource? {
        if case .connected(let t) = connection {
            return t
        }
        
        return nil
    }
    
}


public final class PortObserver : ObservableObject {
    @Published public var position: CGPoint = .zero
}


@propertyWrapper
public class Input<Value:Parametable> : Codable, ParameterWrapper, DynamicProperty {
    public var portObserver: PortObserver {observer}
    
    public var position: CGPoint {
        get{ observer.position }
        set(v){ observer.position = v }
    }
    
    @ObservedObject var observer : PortObserver = .init()
    
    public var defaultName: String = ""
    
    public let id = UUID()
    public var displayName: String = ""
    
    public var portType: ParameterWrapperType {.input}
    public var valueType: Parametable.Type {return type(of:wrappedValue)}
    public var connection: Connection = .none
    
    public var wrappedValue : Value
    public var projectedValue : ParameterWrapper { get{ self }  }
    
    
    
    public init(wrappedValue initialValue:Value){
        wrappedValue = initialValue
    }
    
    
    enum Key : CodingKey {
        case valueData
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let data = try container.decode(Data.self, forKey: .valueData)
        wrappedValue = try Value.instantiate(from: data)
              
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        try container.encode(wrappedValue.encoded(), forKey: .valueData)
    }
    
    
    func createPublishedParameter() -> ParameterWrapper {
        let input = Input(wrappedValue: wrappedValue)
        return input
    }
    
    
    
}



@propertyWrapper
public class Output<Value:Parametable> : Codable, ParameterWrapper, DynamicProperty {
    public var portObserver: PortObserver {
        observer
    }
    
    
    public var position: CGPoint {
        get{ observer.position }
        set(v){ observer.position = v }
    }
    
    @ObservedObject var observer : PortObserver = .init()
    
    public let id = UUID()
    public var displayName: String = ""
    public var defaultName : String = ""
    
    public var portType: ParameterWrapperType {.output}
    public var valueType: Parametable.Type {return type(of:wrappedValue)}
    public var connection: Connection = .none
    
    public var wrappedValue : Value
    public var projectedValue : Output<Value> { get{ self }  }
    
    
    
    
    enum Key : CodingKey {
        case valueData
    }
    
    public init(wrappedValue initialValue:Value){
        wrappedValue = initialValue
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let data = try container.decode(Data.self, forKey: .valueData)
        wrappedValue = try Value.instantiate(from: data)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        try container.encode(wrappedValue.encoded(), forKey: .valueData)
    }
    
    
    
    
    
}





