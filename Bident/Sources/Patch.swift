//
//  Patch.swift
//  Bident
//
//  Created by 稲田成実 on 2020/03/19.
//  Copyright © 2020 Narumi Inada. All rights reserved.
//

import Foundation
import SwiftUI







//MARK: - Patch protocol

public protocol Patch {
    
    
    var patchType : PatchType {get}
    var timeMode : PatchTimeMode {get}
    
    
    var info : PatchInfo {get set}
    
    init()
    
    func execute(context:inout PropagateContext)
    
    
}



