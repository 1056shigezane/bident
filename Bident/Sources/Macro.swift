//
//  Macro.swift
//  Bident
//
//  Created by Narumi Inada on 2020/04/06.
//  Copyright © 2020 Narumi Inada. All rights reserved.
//

import Foundation
import SwiftUI
import Combine



public protocol Macro : Patch, ParameterDefinable {
    
    var children : [Patch] {get set}
    
}





extension Macro {
    
    
    /**
     If return is nil, the parameter is not existing in this macro.
     Do disconnect.
     */
    
    func patch(for id:UUID) -> Patch? {
        children.first { $0.info.id == id }
    }
    
    
    func execute() {
        startPropagate { context in
            
        }
    }
    
    func contains(it:Patch) -> Bool {
        guard it.id != self.id else{ return true }
        
        if isParentOf(patch: it) {
            return true
        }
        
        for state in children {
            
            if let macro = state as? Macro {
                if macro.contains(it: it) {
                    return true
                }
            }
        }
        
        return false
    }
    
    
    
    public mutating func set(patch:Patch) {
        
        
        
        if self.id == patch.id {
            
            self = patch as! Self
            return
        }
        
        if let idx = children.firstIndex(where: { child -> Bool in
            child.id == patch.id
        }) {
            
            children[idx] = patch
            
        }else{
            
            for idx in 0 ..< children.count {
                let id : Int = idx
                if self.children[id] is Macro {
                    
                    withUnsafeMutablePointer(to: &self.children[id]) { ptr in
                        ptr.withMemoryRebound(to: Macro.self, capacity: 1) { mptr in
                            mptr.pointee.set(patch: patch)
                        }
                    }
                }
            }
        }
    }
    
    public func isParentOf(patch:Patch) -> Bool {
        children.contains { child in
            return child.id == patch.id
        }
    }
    
    public mutating func append(child:Patch){
        children.append(child)
    }
    
    
    func searchPatch(for parameter:ParameterWrapper) -> Patch? {
        
        if hasParameter(for: parameter) { return self }
        
        for idx in children.indices {
            
            let child = children[idx]
            if child.hasParameter(for: parameter) {
                return child
            }else{
                if let macro = child as? Macro {
                    return macro.searchPatch(for: parameter)
                }
            }
            
        }
        return nil
    }
    
    func codes() -> [Cable] {
        var connections : [(src:ParameterWrapper,dst:ParameterWrapper)] = []
        for idx in children.indices {
            let thisConnections : [(src:ParameterWrapper,dst:ParameterWrapper)] = children[idx].parameters(for: [.input,.customInput]).compactMap { (label,parameter) in
                if case .connected(let dst) = parameter.connection, let dstParameter = dst.parameter {
                    return (parameter,dstParameter)
                }
                return nil
            }
            
            connections.append(contentsOf: thisConnections)
        }
        
        return connections.map { (src,dst) in
            Cable.init(start: src.portObserver, end: dst.portObserver)
        }
        
    }
    
    func startPropagate(completion:@escaping (PropagateContext) -> Void) {
        
        let consumers : [Patch] = children.compactMap { patch in
            if patch.patchType == .consumer {
                return patch
            }else{ return nil }
        }
        
        
        var context = PropagateContext(parent: self, currentIndex: 0, iterationCount: 1, frameID: UUID(), consumers: consumers)
        
        for i in consumers.count - 1 ... 0 {
            
            var consumer = consumers[i]
            
            consumer.async(context: &context) { iterating in
                
                completion(context)
                
            }
        }
    }
    
    
}











//MARK: Macro View Body


extension Binding : Identifiable where Value == Macro {
    public var id : UUID {wrappedValue.id}

    var macros : [Binding<Macro>] {
        get{

            var indices : [Int] = []

            for id in 0 ..< wrappedValue.children.count {
                if wrappedValue.children[id] is Macro {
                    indices.append(id)
                }
            }

            return indices.map { idx in
                PatchConverter(patch: projectedValue.children[idx]).bindingAsMacro
                //projectedValue.children[idx].
                //.macroConverter.macro
            }
        }
    }
}

extension Binding where Value == Patch {
    var macroConverter : PatchConverter { PatchConverter(patch: self) }
}

class PatchConverter {
    @Binding var src : Patch
    
    init(patch:Binding<Patch>){
        _src = patch
    }
    
    deinit{print("Deinit")}
    var macro : Macro { self.src as! Macro }
    var bindingAsMacro : Binding<Macro> {
        Binding<Macro>.init {
            self.src as! Macro
        } set: { new in
            self.src = new
        }
    }
}






struct MacroOutline : View {
    
    @EnvironmentObject var root : RootEntity
    @Binding var current : Macro
    
    var body : some View {
        EditorView(macro: $current)
    }
}

public struct CompositionEditor : View {
    @ObservedObject var root : RootEntity
    
    //let converter : PatchConverter
    
    public init(root rt:RootEntity){
        
//        if rt.currentPath.isEmpty {
//
//            converter = PatchConverter(patch: rt.rootAsPatch)
//
//        }else{
//
//            var now = PatchConverter(patch: rt.rootAsPatch)
//            for path in rt.currentPath {
//
//                let patch = now.bindingAsMacro.macros.first(where: { child in
//                    child.wrappedValue.id == path
//                })!
//
//                now = PatchConverter(patch: patch)
//            }
//            converter = now
//
//        }
        root = rt
        
    }
    
    
    
    var current : Binding<Macro> {
        print("get current")

        if root.currentPath.isEmpty {

            return $root.rootMacroPatch

        }else{

            var now = $root.rootMacroPatch
            for path in root.currentPath {
                let t = now.macros.first(where: { child in
                    child.wrappedValue.id == path
                })!
                
                
                now = t//PatchConverter(patch: t).bindingAsMacro
            }

            return now
        }
    }
    
    public var body : some View {
        EditorView(macro: current).environmentObject(root)
    }
}

public struct EditorView : View {
    
    @EnvironmentObject var root : RootEntity
    @Binding var macro : Macro
    
    @State var showLibrary : Bool = false
    
    
    public var body : some View {
        editorView
    }
    
    var editorView : some View {
        
        
        return ZStack{
            
            root.mouse
            
            ForEach(macro.children.indices,id:\.self){ idx in
                
                PatchReceptorView(with: _macro.children[idx], macro: $macro)
                    .fixedSize()
                
            }.ignoresSafeArea()
            
            
            overlay
            
        }
    }
    
    var overlay : some View {
        let cds = macro.codes()
        return ZStack{
            
            if !cds.isEmpty {
                ForEach(cds) { cable in// Cables
                    cable.frame(width: nil, height: nil, alignment: .center)
                }.ignoresSafeArea(.all, edges: .all)
            }
            
            VStack{
                HStack{
                    if !root.currentPath.isEmpty {
                        Button {
                            
                            root.currentPath.removeLast()
                            
                        } label: {
                            Image(systemName: "chevron.backward").foregroundColor(.white)
                                .frame(width: 44, height: 44, alignment: .center)
                        }
                        .padding()
                    }
                    Spacer()
                    
                    Button {
                        
                        showLibrary.toggle()
                        
                    } label: {
                        Image(systemName: "rectangle.fill.badge.plus").foregroundColor(.white)
                            .frame(width: 44, height: 44, alignment: .center)
                    }
                    .padding()
                    .sheet(isPresented: $showLibrary) {
                        PatchPlugInView { patch in
                            //root.rootMacroPatch.append(child: patch)
                            macro.append(child: patch)
                        }
                    }
                    
                }
                Spacer()
                
            }
        }
    }
}


public final class RootEntity : ObservableObject {
    
    static var verses : [RootEntity] = []
    
    static func verse(from id:UUID) -> RootEntity? {
        verses.first {
            $0.id == id
        }
    }
    
    @Published public var rootMacroPatch : Macro
    @Published var currentPath : [UUID] = []
    @Published var connectionSession : Connection = .none
    
    var rootAsPatch : Binding<Patch> {
        Binding {
            self.rootMacroPatch
        } set: { (value) in
            self.rootMacroPatch = value as! Macro
        }

    }
    
    var destructor : (Macro) -> Void
    
    
    @ObservedObject var mouse : MouseMonitor = .init()
    
    
    let id : UUID = .init()
    
    
    private func hold() {
        Self.verses.append(self)
    }
    
    static public func createNew() -> Macro {
        let new = RootEntity { (_) in
        }
        return new.rootMacroPatch
    }
    
    
    
    public func destruct() {
        let least = rootMacroPatch
        while let index = Self.verses.firstIndex(where: { (entity) -> Bool in
            entity.rootMacroPatch.id == least.id
        }) {
            Self.verses.remove(at: index)
        }
    }
    
    public init(with initialValue:MacroPatch,destructor src:@escaping (Macro)->Void){
        rootMacroPatch = initialValue
        //currentMacro = initialValue
        destructor = src
        hold()
    }
    
    public init(destructor src:@escaping (Macro)->Void) {
        let initial = MacroPatch()
        rootMacroPatch = initial
        //currentMacro = initial
        destructor = src
        hold()
    }
    private init(){
        let initial = MacroPatch()
        rootMacroPatch = initial
        //currentMacro = initial
        destructor = {_ in}
        hold()
    }
    deinit {
        let least = rootMacroPatch
        destructor(least)
    }
}






public struct MacroPatch : Macro {
    
    public var connectionSession: Connection = .none
    
    public var children: [Patch] = []
//    public var childrenState: State<[Patch]> {
//        get{_children}
//        set{_children = newValue}
//    }
    
    public var customInputs: [ParameterWrapper] = []
    public var customOutputs: [ParameterWrapper] = []
    
    mutating func addOutput(parameter: ParameterWrapper) {
        customInputs.append(parameter)
    }
    
    mutating func addInput(parameter: ParameterWrapper) {
        customOutputs.append(parameter)
    }
    
    public var timeMode: PatchTimeMode = .time
    
    public init(){}
    
    public func execute(context:inout PropagateContext){}
    
    public var patchType: PatchType = .macro
    
    public var info: PatchInfo = PatchInfo()
    
 }


struct Cable : View, Identifiable {
    let id = UUID()
    
    @ObservedObject var start : PortObserver
    @ObservedObject var end : PortObserver
    
    
    
    var body : some View {
        let rect = CGRect(origin: start.position, size: end.position.size)
        return Line(position: rect).stroke(Color.green,lineWidth: 4.0)
    }
    
    struct Line : Shape {
        
        var position : CGRect
        var animatableData: CGRect {
            get { position }
            set { position = newValue }
        }
        
        func path(in rect: CGRect) -> Path {
            let start = position.origin
            let end = position.size.point
            var path = Path()
            path.move(to: start)
            path.addLine(to: end)
            
            return path
        }
    }
}

//public struct RootMacroPatch : Macro {
//
//
//
//    public final class Entity : ObservableObject {
//
//        static fileprivate var verse : [RootMacroPatch.Entity] = []
//
//        @Published public var rootMacroPatch : RootMacroPatch
//        @Published public var connectionSession : Connection = .none
//        @Published public var currentMacro : Macro {
//            didSet{
//                rootMacroPatch.set(patch: currentMacro)
//            }
//        }
//
//        var destructor : (RootMacroPatch) -> Void
//
//        private func hold() {
//            Self.verse.append(self)
//        }
//
//        static public func createNew() -> RootMacroPatch {
//            let new = Entity { (_) in
//            }
//            return new.rootMacroPatch
//        }
//
//        public func destruct() {
//            let least = rootMacroPatch
//            while let index = Self.verse.firstIndex(where: { (entity) -> Bool in
//                entity.rootMacroPatch.id == least.id
//            }) {
//                Self.verse.remove(at: index)
//            }
//        }
//
//        public init(with initialValue:RootMacroPatch,destructor src:@escaping (RootMacroPatch)->Void){
//            rootMacroPatch = initialValue
//            currentMacro = initialValue
//            destructor = src
//            hold()
//        }
//
//        public init(destructor src:@escaping (RootMacroPatch)->Void) {
//            let initial = RootMacroPatch()
//            rootMacroPatch = initial
//            currentMacro = initial
//            destructor = src
//            hold()
//        }
//        private init(){
//            let initial = RootMacroPatch()
//            rootMacroPatch = initial
//            currentMacro = initial
//            destructor = {_ in}
//            hold()
//        }
//        deinit {
//            let least = rootMacroPatch
//            destructor(least)
//        }
//    }
//
//    static func patch(for param:ParameterWrapper) -> Patch? {
//
//        for root in RootMacroPatch.verse {
//            if let found = root.rootMacroPatch.searchPatch(for:param ) {
//                return found
//            }
//        }
//        return nil
//
//    }
//
//
//    static var verse : [RootMacroPatch.Entity] {
//        RootMacroPatch.Entity.verse
//    }
//
//    public var entity : Entity {
//        Self.verse.first(where: { (entity) -> Bool in
//        entity.rootMacroPatch.id == self.id
//        })! }
//
//
//
//    public init(){}
//
//
//    public func execute(context: inout PropagateContext) {
//
//    }
//
//
//
//
//
//    public var connectionSession : Connection {
//        get{entity.connectionSession}
//        set{entity.connectionSession = newValue}
//    }
//
//    public var children: [Patch] = []
//
//    public var patchType: PatchType = .undefined
//
//    public var timeMode: PatchTimeMode = .time
//
//    public var info: PatchInfo = .init()
//
//    public var customOutputs: [ParameterWrapper] = []
//
//    public var customInputs: [ParameterWrapper] = []
//
//    var currentPath : [UUID] = []
//}
//
//extension RootMacroPatch : PatchView {
//    public var id : UUID {info.id}
//    public var body : some View {
//        let obs = ObservedObject<RootMacroPatch.Entity>.init(wrappedValue: entity)
//        editorView(root: entity,macro: obs.projectedValue.rootMacroPatch)
//    }
//
//
//
//
//    var currentMacro : Macro {
//        let obs = ObservedObject<RootMacroPatch.Entity>.init(wrappedValue: entity)
//
//        var now : Binding<Macro> = obs.projectedValue.rootMacroPatch
//
//        for path in currentPath {
//
//            let a = now.children[0]
//            guard let index = now.children.wrappedValue.firstIndex(where: { (child) -> Bool in
//                child.id == path
//            }) else{break}
//
//            now = now.children[index] as! Binding<Macro>
//
//        }
//
//
//    }
//
//
//
//}
