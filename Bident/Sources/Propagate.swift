//
//  Propagate.swift
//  Bident
//
//  Created by Narumi Inada on 2020/03/22.
//  Copyright © 2020 Narumi Inada. All rights reserved.
//

import Foundation
import CoreGraphics

public enum Connection : Codable {
    
    public struct ConnectionSource {
        public var patch : Patch?
        public weak var parameter : ParameterWrapper?
        
        init(parameter param:ParameterWrapper, patch t:Patch ){
            parameter = param
            patch = t//RootMacroPatch.patch(for: param)
        }
        
        func paramerPosition() -> CGPoint? {
            guard let param = parameter else{return nil}
            
            return param.position
        }
    }
    
    case none
    case initialized(patchID:UUID,paramID:UUID)
    case connecting(ConnectionSource)
    case connected(ConnectionSource)
   
    
    enum Key : CodingKey {
        case raw
        case patchID
        case paramID
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        
        switch self {
        case .none:
            try container.encode( 0, forKey: .raw)
            break
            
        case .initialized(let patchID, let paramID):
            try container.encode(1, forKey: .raw)
            try container.encode(patchID, forKey: .patchID)
            try container.encode(paramID, forKey: .paramID)
            break
        
        case .connecting(let reference):
            guard let patch = reference.patch, let param = reference.parameter else{
                try container.encode( 0, forKey: .raw)
                break
            }
            
            try container.encode(1, forKey: .raw)
            try container.encode(patch.info.id, forKey: .patchID)
            try container.encode(param.id, forKey: .paramID)
            break
        case .connected(let reference):
            
            guard let patch = reference.patch, let param = reference.parameter else{
                try container.encode( 0, forKey: .raw)
                break
            }
            
            try container.encode(1, forKey: .raw)
            try container.encode(patch.info.id, forKey: .patchID)
            try container.encode(param.id, forKey: .paramID)
            
            break
            
        
        }
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: Key.self)
        
        let raw = try container.decode(Int.self, forKey: .raw)
        
        switch raw {
        case 0:
            self = .none
            
        case 1:
            let patchID = try container.decode(UUID.self, forKey: .patchID)
            let paramID = try container.decode(UUID.self, forKey: .paramID)
            
            self = .initialized(patchID: patchID, paramID: paramID)
            
        default:
            fatalError("Exception, initialize Connection")
        }
    }
    public typealias PortDestination = (patch:Patch,parameter:ParameterWrapper)
    
    public var destination : PortDestination? {
        if case .connected(let src) = self, let patch = src.patch, let param = src.parameter {
            return (patch,param)
        }
        return nil
    }
    
    var destinationsPoint : CGPoint? {
        if case .connected(let src) = self, let param = src.parameter {
            return param.position
        }
        
        return .zero
    }
    
}





/**
 Used in Propagate function
 
 Parent Pointer is temporal.
 Do not use persistently.
 */


public struct PropagateContext {
    
    //Forward
    let parent : Macro
    var currentIndex : Int
    var iterationCount : Int
    var frameID : UUID
    
    let consumers : [Patch]
    
    
}





extension Patch {
    
    
    func getDependees(parent: Macro) -> [Patch] {
        
        return parameters(for: [.input,.customInput]).compactMap { (_, param) in
            
            
            
            switch param.connection {
                
            case .none,.connecting(_):
                
                return nil
                
            case .initialized(let patchID,let paramID):
                
                guard patchID != parent.id else{ return nil }
                
                if let source = parent.patch(for: patchID),
                    let param = source.parameter(for: paramID) {
                        
                    param.connection = Connection.connected(.init(parameter: param,patch: source))
                    
                    return source
                    
                }else{
                    
                    param.connection = .none
                    return nil
                    
                }
                
            case .connected(let source):
                
                
                
                if let src = source.patch, src.info.id != parent.id {
                    
                    return src
                    
                }else{
                    
                    param.connection = .none
                    return nil
                    
                }
                
            }
            
        }
        
    }
    
    
    
    
    mutating func setFrameID(context:PropagateContext, isIterating:Bool) {
        
        if context.currentIndex == 0 {
            
            info.lastExecutedFrameID = FrameID(id: context.frameID, isIterating: isIterating, index: context.currentIndex)
            
        }else{
            info.lastExecutedFrameID = FrameID(id: context.frameID, isIterating: info.lastExecutedFrameID.isIterating, index: context.currentIndex)
        }
        
    }
    
    
    func isAlreadyCalled(in context:PropagateContext) -> Bool {
        
        if context.currentIndex > 0 {
            
            return self.info.lastCalledFrameID.id == context.frameID
            
        }else{
            
            guard self.info.lastCalledFrameID.id == context.frameID else{return false}
            
            if self.info.lastExecutedFrameID.isIterating {
                return self.info.lastCalledFrameID.index == context.currentIndex
            }else{
                return true
            }
            
        }
        
        
        
    }
    
    func isAlreadyExecuted(in context:PropagateContext) -> Bool {
        
        if context.currentIndex > 0 {
            
            // in Least Secondary Iteration
            
            return self.info.lastExecutedFrameID.id == context.frameID
            
        }else{
            
            //First Iteration Or Single Iterarion
            
            guard self.info.lastExecutedFrameID.id == context.frameID else{return false}
            
            if self.info.lastExecutedFrameID.isIterating {
                
                return self.info.lastExecutedFrameID.index == context.currentIndex
                
            }else{
                
                return true
                
            }
        }
        
    }
    
    
    mutating func sync(context:inout PropagateContext, completion:(inout PropagateContext)->Void) {
        var dependees = getDependees(parent: context.parent)
        for idx in dependees.indices {
            var dependee : Patch = dependees[idx]
            
            
            
            dependee.execute(context: &context)
            dependee.sync(context: &context, completion: { context in
                
                
                
            })
            dependees[idx] = dependee
        }
        completion(&context)
    }
    
    mutating func async(context:inout PropagateContext,addingDependees:[Patch] = [], completion: @escaping (_ iterating:Bool) -> Void ){
        
        let queue = self.queue
        
        
        
        guard !self.isAlreadyExecuted(in: context) else{
            completion(self.info.lastExecutedFrameID.isIterating)
            return
        }
        
        guard !self.isAlreadyCalled(in: context) else{
            self.info.waitingompletionsForComplete.append( completion )
            return
        }
        
        
        
        var dependees : [Patch] = self.getDependees(parent: context.parent)
            .compactMap { patch in
                guard patch.info.lastExecutedFrameID.id != context.frameID,
                      patch.id != context.parent.id
                else{ return nil }
                return patch
            }
        
        dependees.append(contentsOf:addingDependees)
        
        
        if self.patchType == .consumer {
            
            ///Guarantee atomic rendering order
            
            let consumers : [Patch] = context.parent.children.compactMap { patch in
                if patch.patchType == .consumer {
                    return patch
                }else{ return nil }
            }.sorted { (l, r) -> Bool in
                l.info.order < r.info.order
            }
            
            
            if let selfIndex = consumers.firstIndex(where: { patch in
                patch.id == self.id
            }){
                let idx = consumers.index(before: selfIndex)
                
                dependees.append(consumers[idx])
            }
            
            
            
            
        }
        
        ///if no dependees, this is　source leaf. do dispatch.
        
        if dependees.isEmpty {
            
            self.execute(context: &context)
            completion(false)
            
        }else{
            
            var dependingCount = dependees.count
            var isIterating : Bool = false
            var context = context
            
            dependees.indices.forEach { index in
                var patch = dependees[index]
                
                let blk : (_ patch:inout Patch, _ iterating:Bool) -> Void = { _patch, iterating in
                    isIterating = isIterating || iterating
                    dependingCount -= 1
                    
                    if dependingCount == 0 {
                        
                        _patch.execute(context: &context)
                        
                        _patch.setFrameID(context: context, isIterating: isIterating)
                        
                        completion(iterating)
                        
                        for blks in _patch.info.waitingompletionsForComplete {
                            blks(iterating)
                        }
                    }
                }
                
                patch.async(context: &context) { iterating in
                    
                    queue.async {
                        blk(&patch,iterating)
                        dependees[index] = patch
                    }
                }
            }
        }
    }
}





