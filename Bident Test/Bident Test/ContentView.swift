//
//  ContentView.swift
//  Bident Test
//
//  Created by 稲田成実 on 2020/03/19.
//  Copyright © 2020 Narumi Inada. All rights reserved.
//

import SwiftUI
import Bident

class Shared : ObservableObject {
    static let shared = Shared()
    
    var root : Macro { get{entity.rootMacroPatch} set(v){entity.rootMacroPatch = v}}
    @Published var entity : RootEntity = RootEntity(destructor: {_ in})
    
    init(){
        let test = Adder()
        root.children.append(test)
        
        print(test.parameters(for: [.input]))
    }
}





struct ContentView: View {
    
    
    
    //@ObservedObject var shared = Shared.shared
    @ObservedObject var entity : RootEntity = Shared.shared.entity
    
    
    var body: some View {
        ZStack{
            Color.black
            
            CompositionEditor(root:entity)
           
            
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

